import PropTypes from 'prop-types';
import { useCallback } from 'react';
import RRPT from 'react-router-prop-types';

const BoardUrlManager = ({ match: { params: { boardId } }, history, children }) => {
  const setBoardId = useCallback((id) => {
    if (id === null) {
      history.push('/');
    } else if (id && id !== boardId) {
      history.push(`/${id}`);
    }
  }, [boardId, history]);

  return children({ boardId, setBoardId });
};

BoardUrlManager.propTypes = {
  match: RRPT.match.isRequired,
  history: RRPT.history.isRequired,
  children: PropTypes.func.isRequired,
};
export default BoardUrlManager;
