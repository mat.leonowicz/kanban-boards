// eslint-disable-next-line import/no-extraneous-dependencies
import { createMemoryHistory } from 'history';
import React from 'react';
import { Route, Router } from 'react-router-dom';
import BoardUrlManager from './BoardUrlManager';

function renderBoardManagerWithRouter(children, { initialRoute = '/' } = {}) {
  const history = createMemoryHistory({ initialEntries: [initialRoute] });

  const r = render(
    <Router history={history}>
      <Route
        path="/:boardId?"
        render={props => (
          <BoardUrlManager {...props}>{children}</BoardUrlManager>
        )}
      />
    </Router>,
  );
  return {
    ...r,
    history,
  };
}

it('initial state', function () {
  let renderPropArg;
  const renderFn = (receivedArg) => {
    renderPropArg = receivedArg;
    return null;
  };
  renderBoardManagerWithRouter(renderFn, { initialRoute: '/abc' });

  expect(renderPropArg.boardId).toEqual('abc');
});

it('manipulation via url', function () {
  let renderPropArg;
  const renderFn = (receivedArg) => {
    renderPropArg = receivedArg;
    return null;
  };
  const { history } = renderBoardManagerWithRouter(renderFn);

  history.push('/xyz');

  expect(renderPropArg.boardId).toEqual('xyz');
});

it('manipulation via provided method', function () {
  let renderPropArg;
  const renderFn = (receivedArg) => {
    renderPropArg = receivedArg;
    return null;
  };
  const { history } = renderBoardManagerWithRouter(renderFn);
  const historyPushSpy = jest.spyOn(history, 'push');

  renderPropArg.setBoardId('xyz');
  expect(renderPropArg.boardId).toEqual('xyz');

  renderPropArg.setBoardId(null);
  expect(renderPropArg.boardId).toEqual(undefined);
  expect(historyPushSpy).toHaveBeenCalledWith('/');
});
