import { createGlobalStyle } from 'styled-components';
import styledNormalize from 'styled-normalize';
import color from './constants/color';
import { fontFamily, fontSize, fontWeight } from './constants/font';

const GlobalStyles = createGlobalStyle`
  ${styledNormalize};
  html {
    height: 100%;
    box-sizing: border-box;
  }
  body {
    height: 100%;
    margin: 0;
    padding: 0;
    font-family: ${fontFamily};
    font-size: ${fontSize.default};
    font-weight: ${fontWeight.default};
    line-height: 1.5;
    background-color: ${color.pageBackground};
    color: ${color.fontDefault};
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
  };
  * {
    &,
    &:before,
    &:after {
      box-sizing: inherit;
    }
  }
  
  a {
    text-decoration: none;
    &:hover,
    &:active,
    &:focus {
      text-decoration: none;
    }
  }
  
  p, h1, h2, h3, h4, h5, h6 {
    margin: 0;
  }
  
  ul, ol {
    list-style: none;
    margin: 0;
    padding: 0;
    border: 0;
    font: inherit;
    font-size: 100%;
    vertical-align: baseline;
  }
`;

export default GlobalStyles;
