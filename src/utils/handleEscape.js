const handleEscape = action => (e) => {
  if (action && e.key === 'Escape') {
    action();
  }
};

export default handleEscape;
