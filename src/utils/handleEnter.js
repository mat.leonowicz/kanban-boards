const handleEnter = action => (e) => {
  if (action && e.key === 'Enter') {
    action();
  }
};

export default handleEnter;
