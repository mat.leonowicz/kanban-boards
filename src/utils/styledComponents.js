import { css } from 'styled-components';
import { spacingUnitInt } from '../constants/style';

/**
 * These here are helper functions to more easily create styled-components
 */

/** camelCaseToDash('marginBottom') = 'margin-bottom' */
function camelCaseToDash(str) {
  return str.replace(/([a-zA-Z])(?=[A-Z])/g, '$1-').toLowerCase();
}

/** ${styleCss('marginBottom')} = 'margin-bottom: ${props => props.marginBottom}' */
export const styleCss = (propName, cssPropName) =>
  props => props[propName] && css`
    ${cssPropName || camelCaseToDash(propName)}: ${props[propName]}
  `;

/** Same as styleCss but, if the value is a number, it is treated as spacingUnits, rather then exact value */
export const styleCssSpacing = (propName, cssPropName) =>
  props => hasValueProvided(props[propName]) && css`
    ${cssPropName || camelCaseToDash(propName)}: ${su(props[propName])};
  `;

/** su stands for for spacing units */
export const su = (value) => {
  if (typeof value === 'number') {
    return `${spacingUnitInt * value}px`;
  }
  return value;
};

const hasValueProvided = value => value || value === 0;
