export const isListTitleValid = listTitle => listTitle.length > 0 && listTitle.length <= 30;

export const INVALID_LIST_TITLE = 'List title is required (max 30 characters)';
