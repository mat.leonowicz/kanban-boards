import { act, renderHook } from '@testing-library/react-hooks';
import uuid from 'uuid/v4';
import { createBoard, createCard, createList } from '../__test__/testUtils';
import useKanbanState from './useKanbanState';

describe('useKanbanState', () => {
  const board1 = createBoard(uuid());
  const board2 = createBoard(uuid());
  const list1 = createList(uuid(), board1.id);
  const list2 = createList(uuid(), board2.id);
  const card1 = createCard(uuid(), list1.id);
  const card2 = createCard(uuid(), list2.id);
  const testState = {
    boards: {
      [board1.id]: board1,
      [board2.id]: board2,
    },
    lists: {
      [list1.id]: list1,
      [list2.id]: list2,
    },
    cards: [
      card1,
      card2,
    ],
  };

  afterEach(() => {
    window.localStorage.removeItem('data');
  });

  it('should properly initialize', () => {
    const { result } = renderHook(() => useKanbanState());

    // act(() => {
    //   result.current.increment();
    // });

    expect(result.current.boards).toStrictEqual({});
    expect(result.current.lists).toStrictEqual({});
    expect(result.current.cards).toStrictEqual([]);
  });

  describe('add operations', () => {
    it('adds board', () => {
      const { result } = renderHook(() => useKanbanState());

      const title = 'some title';
      let id;
      act(() => {
        id = result.current.addBoard(title);
      });

      expect(result.current.boards[id]).toEqual({
        id,
        title,
      });
    });

    it('adds list', () => {
      const { result } = renderHook(() => useKanbanState(testState));

      const title = 'some title';
      let id;
      act(() => {
        id = result.current.addList(board1.id, title);
      });

      expect(result.current.lists[id]).toEqual({
        id,
        title,
        boardId: board1.id,
      });
    });

    it('adds card', () => {
      const { result } = renderHook(() => useKanbanState(testState));

      const title = 'some title';
      const description = 'some description';
      let id;
      act(() => {
        id = result.current.addCard(list1.id, { title, description });
      });

      expect(result.current.cards.find(card => card.id === id)).toEqual({
        id,
        title,
        description,
        listId: list1.id,
      });
    });
  });

  describe('delete operations', () => {
    it('deleteBoard', () => {
      const { result } = renderHook(() => useKanbanState(testState));

      act(() => {
        result.current.deleteBoard(board1.id);
      });

      expect(Object.values(result.current.boards)).not.toContain(board1);
      expect(Object.values(result.current.boards)).toContain(board2);
      expect(Object.values(result.current.lists)).not.toContain(list1);
      expect(Object.values(result.current.lists)).toContain(list2);
      expect(result.current.cards).not.toContain(card1);
      expect(result.current.cards).toContain(card2);
    });

    it('deleteList', () => {
      const { result } = renderHook(() => useKanbanState(testState));

      act(() => {
        result.current.deleteList(list1.id);
      });

      expect(Object.values(result.current.boards)).toContain(board1);
      expect(Object.values(result.current.boards)).toContain(board2);
      expect(Object.values(result.current.lists)).not.toContain(list1);
      expect(Object.values(result.current.lists)).toContain(list2);
      expect(result.current.cards).not.toContain(card1);
      expect(result.current.cards).toContain(card2);
    });

    it('deleteCard', () => {
      const { result } = renderHook(() => useKanbanState(testState));

      act(() => {
        result.current.deleteCard(card1.id);
      });

      expect(Object.values(result.current.boards)).toContain(board1);
      expect(Object.values(result.current.boards)).toContain(board2);
      expect(Object.values(result.current.lists)).toContain(list1);
      expect(Object.values(result.current.lists)).toContain(list2);
      expect(result.current.cards).not.toContain(card1);
      expect(result.current.cards).toContain(card2);
    });
  });

  it('editList', function () {
    const { result } = renderHook(() => useKanbanState(testState));

    const newTitle = 'new title';
    act(() => {
      result.current.editList(list1.id, newTitle);
    });

    expect(Object.values(result.current.lists).find(list => list.id === list1.id)).toStrictEqual({
      ...list1,
      title: newTitle,
    });
  });
});
