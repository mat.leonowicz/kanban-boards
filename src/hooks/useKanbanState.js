import { fromPairs } from 'lodash';
import { useCallback, useMemo } from 'react';
import useLocalStorage from 'react-use/lib/useLocalStorage';
import uuid from 'uuid/v4';

const defaultInitialData = {
  boards: {},
  lists: {},
  cards: [],
};

export default function useKanbanState(initialData = defaultInitialData) {
  const [data, setData] = useLocalStorage('data', initialData);

  const addBoard = useCallback((title) => {
    const id = uuid();
    setData(prevdata => ({
      ...prevdata,
      boards: {
        ...prevdata.boards,
        [id]: {
          id,
          title,
        },
      },
    }));
    return id;
  }, [setData]);

  const deleteBoard = useCallback((idToDelete) => {
    setData((prevData) => {
      const listsIdsToDelete = new Set(
        Object.values(prevData.lists)
          .filter(list => list.boardId === idToDelete)
          .map(list => list.id),
      );
      const cardsIdsTotDelete = new Set(
        prevData.cards
          .filter(card => listsIdsToDelete.has(card.listId))
          .map(card => card.id),
      );
      return {
        boards: fromPairs(Object.entries(prevData.boards).filter(([id]) => id !== idToDelete)),
        lists: fromPairs(Object.entries(prevData.lists).filter(([id]) => !listsIdsToDelete.has(id))),
        cards: prevData.cards.filter(card => !cardsIdsTotDelete.has(card.id)),
      };
    });
  }, [setData]);

  const addList = useCallback((boardId, title) => {
    const id = uuid();
    setData(prevData => ({
      ...prevData,
      lists: {
        ...prevData.lists,
        [id]: {
          id,
          boardId,
          title,
        },
      },
    }));
    return id;
  }, [setData]);

  const editList = useCallback((id, title) => {
    setData(prevData => ({
      ...prevData,
      lists: {
        ...prevData.lists,
        [id]: {
          ...prevData.lists[id],
          title,
        },
      },
    }));
  }, [setData]);

  const deleteList = useCallback((idToDelete) => {
    setData((prevData) => {
      const cardsIdsTotDelete = new Set(
        prevData.cards
          .filter(card => card.listId === idToDelete)
          .map(card => card.id),
      );
      return {
        ...prevData,
        lists: fromPairs(Object.entries(prevData.lists).filter(([id]) => id !== idToDelete)),
        cards: prevData.cards.filter(card => !cardsIdsTotDelete.has(card.id)),
      };
    });
  }, [setData]);

  const addCard = useCallback((listId, { title, description }) => {
    const id = uuid();
    setData(prevData => ({
      ...prevData,
      cards: [...prevData.cards, {
        id,
        listId,
        title,
        description,
      }],
    }));
    return id;
  }, [setData]);

  const deleteCard = useCallback((idToDelete) => {
    setData(prevData => ({
      ...prevData,
      cards: prevData.cards.filter(card => card.id !== idToDelete),
    }));
  }, [setData]);

  return useMemo(() => ({
    boards: data.boards,
    lists: data.lists,
    cards: data.cards,
    addBoard,
    deleteBoard,
    addList,
    editList,
    deleteList,
    addCard,
    deleteCard,
  }), [data.boards, data.cards, data.lists, addBoard, addCard, addList, editList, deleteBoard, deleteCard, deleteList]);
}
