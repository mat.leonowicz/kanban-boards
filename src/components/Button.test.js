import React from 'react';
import Button from './Button';

it('snapshot margins', function () {
  const { container } = render(<Button marginRight={1} marginBottom={1}>Test</Button>);
  expect(container).toMatchSnapshot();
});

it('snapshot bgColor color', function () {
  const { container } = render(<Button bgColor="#333333" color="#444444">Test</Button>);
  expect(container).toMatchSnapshot();
});

it('snapshot disabled', function () {
  const { container } = render(<Button disabled>Test</Button>);
  expect(container).toMatchSnapshot();
});
