import React from 'react';
import PageContainer from './PageContainer';

it('snapshot ', function () {
  const { container } = render(<PageContainer>test</PageContainer>);
  expect(container).toMatchSnapshot();
});
