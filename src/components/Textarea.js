import styled from 'styled-components';
import color from '../constants/color';
import { su } from '../utils/styledComponents';

const Textarea = styled.textarea.attrs(props => ({
  rows: props.rows || 2,
}))`
  display: block;
  width: 100%;
  padding: ${su(2)} ${su(3)};
  border-radius: 5px;
  outline: none;
  line-height: 1.5;
  border: 1px solid ${color.inputBorder};
  &:focus {
    border: 1px solid ${color.accent};
  }
`;

export default Textarea;
