import styled from 'styled-components';
import { su } from '../utils/styledComponents';

const PageContainer = styled.div`
  margin: 0 auto;
  max-width: 1400px;
  padding: ${su(10)}
`;

export default PageContainer;
