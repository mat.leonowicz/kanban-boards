import { darken, transparentize } from 'polished';
import PropTypes from 'prop-types';
import styled, { css } from 'styled-components';
import color from '../constants/color';
import { fontFamily, fontSize, fontWeight } from '../constants/font';
import { defaultBorderRadius } from '../constants/style';
import handleEnter from '../utils/handleEnter';
import { PTchildren, PTspacing } from '../utils/propTypes';
import { styleCssSpacing, su } from '../utils/styledComponents';

const Button = styled.button.attrs(props => ({
  // a11y
  tabIndex: '0',
  ...(props.onClick ? { onKeyDown: handleEnter(props.onClick) } : {}),
}))`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 36px;
  padding: ${su(2)} ${su(4)};
  border-radius: ${defaultBorderRadius};
  border: none;
  
  color: ${color.buttonFont};
  font-size: ${fontSize.small};
  font-weight: ${fontWeight.bold};
  font-family: ${fontFamily};
  line-height: 1;
  text-transform: uppercase;
  letter-spacing: 0.5px;
  
  cursor: pointer;
  background-color: ${props => props.bgColor};
  ${props => !props.disabled && css`
    &:hover {
      background-color: ${darken(0.1, props.bgColor)};
    }
  `};
  ${props => props.disabled && css`
    background-color: ${transparentize(0.5, props.bgColor)};
    cursor: auto;  
  `};
  
  ${props => props.small && css`
    height: 24px;
    padding: 4px 8px;
    letter-spacing: 0;
  `};
  
  ${styleCssSpacing('width')};
  ${styleCssSpacing('margin')};
  ${styleCssSpacing('marginBottom')};
  ${styleCssSpacing('marginRight')};
`;

Button.propTypes = {
  children: PTchildren,
  onClick: PropTypes.func,
  disabled: PropTypes.bool,
  small: PropTypes.bool,
  bgColor: PropTypes.string,
  color: PropTypes.string,
  width: PTspacing,
  margin: PTspacing,
  marginBottom: PTspacing,
  marginRight: PTspacing,
};
Button.defaultProps = {
  bgColor: color.buttonBg,
  color: color.buttonFont,
};
export default Button;
