import PropTypes from 'prop-types';
import styled, { css } from 'styled-components';
import color from '../../constants/color';
import { fontWeight } from '../../constants/font';

const Item = styled.li`
  display: block;
  color: ${color.fontDefault};
  padding: 5px 14px;
  width: 100%;
  user-select: none;
  &:last-of-type {
    border-bottom-left-radius: 4px;
    border-bottom-right-radius: 4px;
  }
  ${props => props.isActive && css`
    background-color: #d2efff;
  `};
  ${props => props.isSelected && css`
    background-color: #d2efff;
    font-weight: ${fontWeight.bold};
  `};
`;

Item.propTypes = {
  isActive: PropTypes.bool,
  isSelected: PropTypes.bool,
};

export default Item;
