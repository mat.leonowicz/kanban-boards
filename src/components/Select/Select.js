import Downshift from 'downshift';
import PropTypes from 'prop-types';
import * as React from 'react';
import Input from '../Input';
import Block from '../layout/Block';
import ArrowIcon from './ArrowIcon';
import ControllerButton from './ControllerButton';
import Item from './Item';
import ItemsList from './ItemsList';

const defaultItemToString = item => item ? item.label : '';
const defaultItemToValue = item => item ? item.value : null;

function stateReducer(state, changes) {
  switch (changes.type) {
    case 'click':
    case Downshift.stateChangeTypes.clickButton:
      return {
        ...changes,
        inputValue: '',
      };
    default:
      return changes;
  }
}

const Select = ({
  items,
  value,
  onChange,
  placeholder,
  itemToLabel = defaultItemToString,
  itemToValue = defaultItemToValue,
}) => {
  const selectedItem = items.find(item => itemToValue(item) === value) || items[0];

  return (
    <Downshift
      onChange={selected => onChange(itemToValue(selected))}
      itemToString={itemToLabel}
      selectedItem={selectedItem}
      stateReducer={stateReducer}
    >
      {({
        getRootProps,
        getInputProps,
        getToggleButtonProps,
        getMenuProps,
        getItemProps,
        inputValue,
        isOpen,
        toggleMenu,
        highlightedIndex,
      }) => (
        <Block relative {...getRootProps()}>
          <Block relative>
            <Input
              {...getInputProps({
                onClick: isOpen ? undefined : toggleMenu,
                active: isOpen,
                placeholder,
              })}
            />
            <ControllerButton {...getToggleButtonProps()}>
              <ArrowIcon isOpen={isOpen} />
            </ControllerButton>
          </Block>
          <Block relative>
            <ItemsList {...getMenuProps({ isOpen })}>
              {isOpen
                ? items
                  .filter(item => itemToLabel(item).toLowerCase().includes(inputValue.toLowerCase()))
                  .map((item, index) => (
                    <Item
                      key={itemToValue(item)}
                      {...getItemProps({
                        item,
                        index,
                        isActive: highlightedIndex === index,
                        isSelected: selectedItem === item,
                      })}
                    >
                      {itemToLabel(item)}
                    </Item>
                  ))
                : null}
            </ItemsList>
          </Block>
        </Block>
      )}
    </Downshift>
  );
};

Select.propTypes = {
  placeholder: PropTypes.string,
  value: PropTypes.any,
  onChange: PropTypes.func.isRequired,
  items: PropTypes.arrayOf(PropTypes.any).isRequired,
  itemToLabel: PropTypes.func,
  itemToValue: PropTypes.func,
};

export default Select;
