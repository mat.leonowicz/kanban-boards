import React from 'react';
import Select from './Select';

it('ui snapshot', function () {
  const { container, getByPlaceholderText } = render(
    <Select
      placeholder="test-placeholder"
      items={[
        { value: '1', label: 'label 1' },
        { value: '2', label: 'label 2' },
        { value: '3', label: 'label 3' },
      ]}
      onChange={jest.fn()}
      value="1"
    />,
  );

  expect(container).toMatchSnapshot();
  fireEvent.click(getByPlaceholderText('test-placeholder'));
  // highlight second option with double arrow down
  fireEvent.keyDown(getByPlaceholderText('test-placeholder'), { keyCode: 40 });
  fireEvent.keyDown(getByPlaceholderText('test-placeholder'), { keyCode: 40 });
  expect(container).toMatchSnapshot();
});
