import PropTypes from 'prop-types';
import React from 'react';

function ArrowIcon({ isOpen }) {
  return (
    <svg
      viewBox="0 0 20 20"
      preserveAspectRatio="none"
      width={16}
      fill="transparent"
      stroke="#979797"
      strokeWidth="2px"
      transform={isOpen ? 'rotate(180)' : undefined}
    >
      <path d="M1,6 L10,15 L19,6" />
    </svg>
  );
}

ArrowIcon.propTypes = {
  isOpen: PropTypes.bool,
};

export default ArrowIcon;
