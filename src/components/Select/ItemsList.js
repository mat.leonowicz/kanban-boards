import PropTypes from 'prop-types';
import styled, { css } from 'styled-components';
import color from '../../constants/color';

const ItemsList = styled.ul`
  ${props => !props.isOpen && css`
    display: none;
  `};
  position: absolute;
  top: 100%;
  z-index: 3;
  transform: translateY(-2px);
  width: 100%;
  max-height: 300px;
  overflow-y: auto;
  padding: 0;
  margin: 0;
  border: 1px solid ${color.inputBorderActive};
  border-bottom-left-radius: 5px;
  border-bottom-right-radius: 5px;
  background: #ffffff;
`;

ItemsList.propTypes = {
  isOpen: PropTypes.bool,
};

export default ItemsList;
