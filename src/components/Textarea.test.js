import React from 'react';
import Textarea from './Textarea';

it('snapshot', function () {
  const { container } = render(<Textarea value="test" onChange={jest.fn()} />);
  expect(container).toMatchSnapshot();
});
