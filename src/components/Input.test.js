import React from 'react';
import Input from './Input';

it('snapshot', function () {
  const { container } = render(<Input value="test" onChange={jest.fn()} />);
  expect(container).toMatchSnapshot();
});

it('snapshot active', function () {
  const { container } = render(<Input value="test" onChange={jest.fn()} active />);
  expect(container).toMatchSnapshot();
});
