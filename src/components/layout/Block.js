import PropTypes from 'prop-types';
import styled, { css } from 'styled-components';
import { PTspacing } from '../../utils/propTypes';
import { styleCss, styleCssSpacing } from '../../utils/styledComponents';

const Block = styled.div`
  ${styleCssSpacing('height')};
  ${styleCssSpacing('minHeight')};
  ${styleCssSpacing('width')};
  ${styleCssSpacing('minWidth')};
  ${styleCssSpacing('maxWidth')};
  
  ${styleCssSpacing('margin')};
  ${styleCssSpacing('marginBottom')};
  ${styleCssSpacing('marginRight')};
  ${styleCssSpacing('marginH', 'margin-left')};
  ${styleCssSpacing('marginH', 'margin-right')};
  
  ${styleCssSpacing('padding')};
  ${styleCssSpacing('paddingLeft')};
  ${styleCssSpacing('paddingRight')};
  ${styleCssSpacing('paddingTop')};
  ${styleCssSpacing('paddingBottom')};
  ${styleCssSpacing('paddingH', 'padding-left')};
  ${styleCssSpacing('paddingH', 'padding-right')};
  
  ${styleCss('bg', 'background')};
  ${styleCss('border')};
  ${styleCss('borderRadius')};
  
  ${props => props.relative && css`
    position: relative;
  `};
  
  ${props => props.clickable && css`
    &:hover {
      cursor: pointer;
    }
  `};
`;
Block.propTypes = {
  height: PTspacing,
  minHeight: PTspacing,
  width: PTspacing,
  maxWidth: PTspacing,
  margin: PTspacing,
  marginBottom: PTspacing,
  marginRight: PTspacing,
  marginH: PTspacing,
  padding: PTspacing,
  paddingLeft: PTspacing,
  paddingRight: PTspacing,
  paddingTop: PTspacing,
  paddingBottom: PTspacing,
  paddingH: PTspacing,
  bg: PropTypes.string,
  border: PropTypes.string,
  relative: PropTypes.bool,
  clickable: PropTypes.bool,
};

export default Block;
