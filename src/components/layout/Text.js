import PropTypes from 'prop-types';
import styled, { css } from 'styled-components';
import color from '../../constants/color';
import { fontSize, fontWeight } from '../../constants/font';
import handleEnter from '../../utils/handleEnter';
import { PTspacing } from '../../utils/propTypes';
import { styleCss, styleCssSpacing } from '../../utils/styledComponents';

const Text = styled.span.attrs(props => ({
  // a11y
  onKeyDown: props.onClick ? handleEnter(props.onClick) : undefined,
  tabIndex: props.onClick ? '0' : undefined,
}))`
  display: block;
  ${props => props.inline && css`
    display: inline-block;
  `};
  ${styleCss('color')};
  ${styleCss('fontFamily')};
  ${styleCss('fontSize')};
  ${styleCss('fontWeight')};
  ${styleCss('textAlign')};
  ${styleCssSpacing('marginBottom')};
  ${styleCssSpacing('marginLeft')};
  ${styleCssSpacing('marginRight')};
  ${props => props.bold && css`
    font-weight: ${fontWeight.bold};
  `};
  ${props => props.onClick && css`
    cursor: pointer;
  `};
`;
Text.defaultProps = {
  color: color.fontDefault,
  fontSize: fontSize.default,
};
Text.propTypes = {
  inline: PropTypes.bool,
  fontFamily: PropTypes.string,
  fontSize: PropTypes.string,
  fontWeight: PropTypes.number,
  bold: PropTypes.bool,
  textAlign: PropTypes.oneOf(['center', 'start', 'end']),
  color: PropTypes.string,
  marginBottom: PTspacing,
  marginLeft: PTspacing,
  marginRight: PTspacing,
  onClick: PropTypes.func,
};
export default Text;
