import PropTypes from 'prop-types';
import styled, { css } from 'styled-components';
import color from '../constants/color';
import { defaultBorderRadius } from '../constants/style';
import { PTspacing } from '../utils/propTypes';
import { styleCssSpacing, su } from '../utils/styledComponents';

const Input = styled.input`
  display: block;
  width: 100%;
  height: 36px;
  padding: ${su(1)} ${su(3)};
  ${props => props.loading && css`
    padding-right: ${su(5)};
  `};
  border-radius: ${defaultBorderRadius};
  outline: none;
  border: 1px solid ${color.inputBorder};
  &:focus {
    border: 1px solid ${color.inputBorderActive};
  }
  ${props => props.active && css`
    border: 1px solid ${color.inputBorderActive};
  `};
  &:disabled {
    background: ${color.inputBgDisabled};
  }
  
  ${props => props.small && css`
     height: 24px;
     padding: ${su(1)} ${su(2)};
  `};
  
  ${styleCssSpacing('marginBottom')}
`;
Input.propTypes = {
  active: PropTypes.bool,
  marginBottom: PTspacing,
};

export default Input;
