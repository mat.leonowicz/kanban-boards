import React from 'react';
import uuid from 'uuid/v4';
import { createBoard, createList } from '../../__test__/testUtils';
import KanbanBoards from './KanbanBoards';

const board1 = createBoard(uuid());
const board2 = createBoard(uuid());
const list11 = createList(uuid(), board1.id);
const list12 = createList(uuid(), board1.id);
const list21 = createList(uuid(), board2.id);
const list22 = createList(uuid(), board2.id);

beforeEach(() => {
  window.localStorage.setItem('data', JSON.stringify({
    boards: {
      [board1.id]: board1,
      [board2.id]: board2,
    },
    lists: {
      [list11.id]: list11,
      [list12.id]: list12,
      [list21.id]: list21,
      [list22.id]: list22,
    },
    cards: [],
  }));
});

afterEach(() => {
  window.localStorage.removeItem('data');
});

it('smoke test', function () {
  render(<KanbanBoards setBoardId={jest.fn} boardId={board1.id} />);
});
