import PropTypes from 'prop-types';
import React, { useCallback } from 'react';
import Block from '../../components/layout/Block';
import Flex from '../../components/layout/Flex';
import FlexItem from '../../components/layout/FlexItem';
import Text from '../../components/layout/Text';
import PageContainer from '../../components/PageContainer';
import color from '../../constants/color';
import { fontSize } from '../../constants/font';
import useKanbanState from '../../hooks/useKanbanState';
import AddList from './components/AddList';
import BoardControls from './components/BoardControls';
import List from './components/List';

const KanbanBoards = ({ boardId, setBoardId }) => {
  const { boards, lists, cards, addBoard, deleteBoard, addList, editList, deleteList, addCard } = useKanbanState();

  const listsToShow = Object.values(lists).filter(list => list.boardId === boardId);
  const addListToBoard = useCallback(title => addList(boardId, title), [addList, boardId]);
  const deleteCurrentBoard = useCallback(() => {
    deleteBoard(boardId);
    const nextBoardIdInLine = Object.keys(boards).filter(id => id !== boardId)[0];
    if (nextBoardIdInLine) {
      setBoardId(nextBoardIdInLine);
    } else {
      setBoardId(null);
    }
  }, [boardId, boards, deleteBoard, setBoardId]);

  const firstBoardId = Object.keys(boards)[0];

  if (boardId && !Object.keys(boards).includes(boardId)) {
    setBoardId(null);
  }

  if (!boardId && firstBoardId) {
    setBoardId(firstBoardId);
  }

  return (
    <PageContainer>
      <Text as="h1" fontSize={fontSize.pageHeader} marginBottom={5}>Kanban boards</Text>
      <Block marginBottom={10}>
        <BoardControls
          boards={boards}
          currentBoardId={boardId}
          setCurrentBoardId={setBoardId}
          addBoard={addBoard}
          deleteBoard={deleteCurrentBoard}
        />
      </Block>
      {boardId && (
        <Flex alignItems="flex-start" minHeight="400px" padding={2} border={`1px dashed ${color.boardBorder}`} style={{ overflow: 'auto' }}>
          {listsToShow.map((list) => {
            const listCards = cards.filter(card => card.listId === list.id);
            const addCardToList = cardData => addCard(list.id, cardData);
            const deleteListFn = () => deleteList(list.id);
            const editListFn = title => editList(list.id, title);
            return (
              <FlexItem margin={2} key={list.id}>
                <List
                  list={list}
                  cards={listCards}
                  addCard={addCardToList}
                  deleteList={deleteListFn}
                  editList={editListFn}
                />
              </FlexItem>
            );
          })}
          <FlexItem padding={2}>
            <AddList addList={addListToBoard} />
          </FlexItem>
        </Flex>
      )}
    </PageContainer>
  );
};

KanbanBoards.propTypes = {
  boardId: PropTypes.string,
  setBoardId: PropTypes.func.isRequired,
};
export default KanbanBoards;
