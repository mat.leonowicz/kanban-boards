import 'jest-styled-components';
import React from 'react';
import { createCard } from '../../../__test__/testUtils';
import Card from './Card';

it('renders Card with title and description', () => {
  const card = createCard('1', 'listId');
  const { container, getByText } = render(<Card card={card} />);
  expect(container.firstChild.childNodes).toHaveLength(2);
  expect(getByText(card.title)).toBeInTheDocument();
  expect(getByText(card.description)).toBeInTheDocument();
  expect(container).toMatchSnapshot();
});

it('renders Card with title only', () => {
  const card = createCard('1', 'listId');
  card.description = '';
  const { container, getByText } = render(<Card card={card} />);
  expect(container.firstChild.childNodes).toHaveLength(1);
  expect(getByText(card.title)).toBeInTheDocument();
  expect(container).toMatchSnapshot();
});
