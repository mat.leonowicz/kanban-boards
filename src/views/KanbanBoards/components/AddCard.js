import PropTypes from 'prop-types';
import React, { useState } from 'react';
import useToggle from 'react-use/lib/useToggle';
import Button from '../../../components/Button';
import Input from '../../../components/Input';
import Block from '../../../components/layout/Block';
import Flex from '../../../components/layout/Flex';
import Text from '../../../components/layout/Text';
import Textarea from '../../../components/Textarea';
import color from '../../../constants/color';
import { fontWeight } from '../../../constants/font';

const AddCard = ({ addCard }) => {
  const [isAdding, toggleIsAdding] = useToggle(false);
  const [title, setTitle] = useState('');
  const [description, setDescription] = useState('');

  const reset = () => {
    toggleIsAdding(false);
    setTitle('');
    setDescription('');
  };

  const handleAdd = () => {
    addCard({
      title,
      description,
    });
    reset();
  };

  if (!isAdding) {
    return (
      <Block paddingLeft={1} paddingTop={4}>
        <Text color={color.accent} fontWeight={fontWeight.medium} onClick={toggleIsAdding}>Add a card...</Text>
      </Block>
    );
  }

  const isValid = title.length > 0;

  return (
    <>
      <Block marginBottom={1} paddingTop={4}>
        <Input
          placeholder="Title"
          autoFocus
          value={title}
          onChange={({ target: { value } }) => setTitle(value)}
        />
      </Block>
      <Block marginBottom={1}>
        <Textarea
          value={description}
          onChange={({ target: { value } }) => setDescription(value)}
          placeholder="Description"
        />
      </Block>
      {!isValid && (
        <Text color={color.error}>Title is required</Text>
      )}
      <Flex>
        <Button small onClick={handleAdd} marginRight={1} disabled={!isValid}>Save</Button>
        <Button small bgColor={color.buttonBgNegative} onClick={reset}>Cancel</Button>
      </Flex>
    </>
  );
};

AddCard.propTypes = {
  addCard: PropTypes.func.isRequired,
};

export default AddCard;
