import React from 'react';
import uuid from 'uuid/v4';
import { createBoard } from '../../../__test__/testUtils';
import BoardControls from './BoardControls';

const board1 = createBoard(uuid());
const board2 = createBoard(uuid());
const boards = {
  [board1.id]: board1,
  [board2.id]: board2,
};

it('shows boards', function () {
  const { getByText, getByLabelText } = render(
    <BoardControls
      addBoard={jest.fn()}
      deleteBoard={jest.fn()}
      currentBoardId={board1.id}
      setCurrentBoardId={jest.fn()}
      boards={boards}
    />,
  );

  fireEvent.click(getByLabelText('open menu'));
  Object.values(boards).forEach((board) => {
    expect(getByText(board.title)).toBeInTheDocument();
  });
});

it('can delete current board', function () {
  const deleteBoard = jest.fn();
  const { getByText } = render(
    <BoardControls
      addBoard={jest.fn()}
      deleteBoard={deleteBoard}
      currentBoardId={board1.id}
      setCurrentBoardId={jest.fn()}
      boards={boards}
    />,
  );

  fireEvent.click(getByText('Delete board'));
  expect(deleteBoard).toHaveBeenCalled();
});

it('can change current board', function () {
  const setCurrentBoardId = jest.fn();
  const { getByText, getByLabelText } = render(
    <BoardControls
      addBoard={jest.fn()}
      deleteBoard={jest.fn()}
      currentBoardId={board1.id}
      setCurrentBoardId={setCurrentBoardId}
      boards={boards}
    />,
  );

  fireEvent.click(getByLabelText('open menu'));
  fireEvent.click(getByText(board2.title));
  expect(setCurrentBoardId).toHaveBeenCalledWith(board2.id);
});

it('can add new board', function () {
  const addBoard = jest.fn();
  const { getByText, getByPlaceholderText, queryByPlaceholderText } = render(
    <BoardControls
      addBoard={addBoard}
      deleteBoard={jest.fn()}
      currentBoardId={board1.id}
      setCurrentBoardId={jest.fn()}
      boards={boards}
    />,
  );

  fireEvent.click(getByText('Add a board'));
  const title = 'some title';
  fireEvent.change(getByPlaceholderText('Board title'), { target: { value: title } });
  fireEvent.click(getByText('Add'));
  expect(addBoard).toHaveBeenCalledWith(title);
  expect(getByText('Add a board')).toBeInTheDocument();
  expect(queryByPlaceholderText('Board title')).not.toBeInTheDocument();
});

it('can cancel adding new board', function () {
  const addBoard = jest.fn();
  const { getByText, getByPlaceholderText, queryByPlaceholderText } = render(
    <BoardControls
      addBoard={addBoard}
      deleteBoard={jest.fn()}
      currentBoardId={board1.id}
      setCurrentBoardId={jest.fn()}
      boards={boards}
    />,
  );

  fireEvent.click(getByText('Add a board'));
  const title = 'some title';
  fireEvent.change(getByPlaceholderText('Board title'), { target: { value: title } });
  fireEvent.click(getByText('Cancel'));
  expect(addBoard).not.toHaveBeenCalled();
  expect(getByText('Add a board')).toBeInTheDocument();
  expect(queryByPlaceholderText('Board title')).not.toBeInTheDocument();
});
