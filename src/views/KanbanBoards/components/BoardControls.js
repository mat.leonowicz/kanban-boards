import PropTypes from 'prop-types';
import React, { useState } from 'react';
import useToggle from 'react-use/lib/useToggle';
import Button from '../../../components/Button';
import Input from '../../../components/Input';
import Block from '../../../components/layout/Block';
import Flex from '../../../components/layout/Flex';
import Select from '../../../components/Select/Select';
import color from '../../../constants/color';
import handleEnter from '../../../utils/handleEnter';

const BoardControls = ({ boards, currentBoardId, setCurrentBoardId, addBoard, deleteBoard }) => {
  const [isAdding, toggleIsAdding] = useToggle(false);
  const [boardTitle, setBoardTitle] = useState('');

  const reset = () => {
    setBoardTitle('');
    toggleIsAdding(false);
  };

  const handleAdd = () => {
    const id = addBoard(boardTitle);
    setCurrentBoardId(id);
    reset();
  };

  return (
    <Flex justifyContent="space-between">
      <Flex alignItems="center">
        {Object.keys(boards).length > 0 && (
          <Block marginRight={4}>
            <Select
              placeholder="Change board"
              value={currentBoardId}
              onChange={setCurrentBoardId}
              items={Object.values(boards).map(board => ({
                value: board.id,
                label: board.title,
              }))}
            />
          </Block>
        )}
        {isAdding ? (
          <Flex alignItems="center">
            <Block marginRight={1}>
              <Input
                placeholder="Board title"
                autoFocus
                value={boardTitle}
                onChange={({ target: { value } }) => setBoardTitle(value)}
                onKeyDown={handleEnter(handleAdd)}
              />
            </Block>
            <Button marginRight={1} onClick={handleAdd}>Add</Button>
            <Button bgColor={color.buttonBgNegative} onClick={reset}>Cancel</Button>
          </Flex>
        ) : (
          <Button onClick={toggleIsAdding}>Add a board</Button>
        )}
      </Flex>
      <Block>
        {currentBoardId && <Button bgColor={color.buttonBgNegative} onClick={deleteBoard}>Delete board</Button>}
      </Block>
    </Flex>
  );
};

BoardControls.propTypes = {
  boards: PropTypes.object.isRequired,
  currentBoardId: PropTypes.string,
  setCurrentBoardId: PropTypes.func.isRequired,
  addBoard: PropTypes.func.isRequired,
  deleteBoard: PropTypes.func.isRequired,
};

export default BoardControls;
