import React from 'react';
import { createCard, createList } from '../../../__test__/testUtils';
import List from './List';

const list = createList('abc', 'xyz');
const cards = [
  createCard('1', 'abc'),
  createCard('2', 'abc'),
];

it('calls editList when editing', () => {
  const editList = jest.fn();
  const { getByDisplayValue, getByTestId } = render(
    <List
      list={list}
      cards={cards}
      addCard={jest.fn()}
      deleteList={jest.fn()}
      editList={editList}
    />,
  );

  fireEvent.click(getByTestId('edit'));
  const input = getByDisplayValue(list.title);

  fireEvent.change(input, { target: { value: 'new title' } });
  fireEvent.click(getByTestId('save'));

  expect(editList).toHaveBeenCalledWith('new title');
  expect(getByTestId('edit')).toBeInTheDocument();
});

it('can cancel editing', () => {
  const editList = jest.fn();
  const { getByText, getByDisplayValue, getByTestId } = render(
    <List
      list={list}
      cards={cards}
      addCard={jest.fn()}
      deleteList={jest.fn()}
      editList={editList}
    />,
  );

  fireEvent.click(getByTestId('edit'));
  const input = getByDisplayValue(list.title);

  fireEvent.change(input, { target: { value: 'new title' } });
  fireEvent.click(getByTestId('cancel'));

  expect(editList).not.toHaveBeenCalled();
  expect(getByText(list.title)).toBeInTheDocument();
  expect(getByTestId('edit')).toBeInTheDocument();
});
