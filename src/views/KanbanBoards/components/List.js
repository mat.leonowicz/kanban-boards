import PropTypes from 'prop-types';
import React, { useState } from 'react';
import useToggle from 'react-use/lib/useToggle';
import styled from 'styled-components';
import { ReactComponent as EditIcon } from '../../../assets/edit.svg';
import { ReactComponent as XIcon } from '../../../assets/x.svg';
import Button from '../../../components/Button';
import Input from '../../../components/Input';
import Block from '../../../components/layout/Block';
import Flex from '../../../components/layout/Flex';
import Text from '../../../components/layout/Text';
import color from '../../../constants/color';
import { fontWeight } from '../../../constants/font';
import { defaultBorderRadius } from '../../../constants/style';
import { su } from '../../../utils/styledComponents';
import { INVALID_LIST_TITLE, isListTitleValid } from '../../../utils/validation';
import AddCard from './AddCard';
import Card from './Card';

const Delete = styled(XIcon)`
  color: ${color.buttonBgNegative};
  width: 14px;
  cursor: pointer;
`;

const Edit = styled(EditIcon)`
  color: ${color.buttonBg};
  width: 18px;
  cursor: pointer;
  margin-right: ${su(2)};
`;

const List = ({ list, cards, addCard, editList, deleteList }) => {
  const [isEditing, toggleIsEditing] = useToggle(false);
  const [listTitle, setListTitle] = useState(list.title);

  const reset = () => {
    toggleIsEditing(false);
    setListTitle(list.title);
  };

  const handleEdit = () => {
    editList(listTitle);
    reset();
  };

  const isValid = isListTitleValid(listTitle);

  return (
    <Block width="300px" padding={3} bg={color.listBg} border={`1px solid ${color.listBorder}`} borderRadius={defaultBorderRadius}>
      <Flex justifyContent="space-between" alignItems="center" marginBottom={3}>
        {isEditing ? (
          <Flex>
            <Block marginRight={1}>
              <Input small value={listTitle} onChange={({ target: { value } }) => setListTitle(value)} />
            </Block>
            <Button
              small
              onClick={handleEdit}
              marginRight={1}
              disabled={!isListTitleValid(listTitle)}
              data-testid="save"
            >
              ✔
            </Button>
            <Button
              small
              bgColor={color.buttonBgNegative}
              onClick={reset}
              marginRight={1}
              data-testid="cancel"
            >
              <XIcon style={{ width: '12px' }} />
            </Button>
          </Flex>
        ) : (
          <Text fontWeight={fontWeight.bold} as="h2">{list.title}</Text>
        )}
        <Flex>
          {!isEditing && <Edit onClick={toggleIsEditing} disabled={!isValid} data-testid="edit" />}
          <Delete onClick={deleteList} data-testid="delete" />
        </Flex>
      </Flex>
      {isEditing && !isValid && (
        <Text color={color.error}>{INVALID_LIST_TITLE}</Text>
      )}
      {cards.map(card => (
        <Card
          key={card.id}
          card={card}
        />
      ))}
      <AddCard addCard={addCard} />
    </Block>
  );
};

List.propTypes = {
  list: PropTypes.object,
  cards: PropTypes.arrayOf(PropTypes.object),
  addCard: PropTypes.func.isRequired,
  editList: PropTypes.func.isRequired,
  deleteList: PropTypes.func.isRequired,
};

export default List;
