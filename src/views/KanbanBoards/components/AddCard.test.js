import React from 'react';
import AddCard from './AddCard';

it('renders and calls addList on save', () => {
  const addCard = jest.fn();
  const { getByText, queryByText, getByPlaceholderText, queryByPlaceholderText } = render(<AddCard addCard={addCard} />);
  expect(getByText('Add a card...')).toBeInTheDocument();

  fireEvent.click(getByText('Add a card...'));
  expect(queryByText('Add a card...')).not.toBeInTheDocument();
  const input = getByPlaceholderText('Title');
  const textarea = getByPlaceholderText('Description');
  const saveButton = getByText('Save');
  expect(input).toBeInTheDocument();
  expect(textarea).toBeInTheDocument();
  expect(saveButton).toBeInTheDocument();
  expect(getByText('Cancel')).toBeInTheDocument();

  const title = 'some title';
  const description = 'some description';
  fireEvent.change(input, { target: { value: title } });
  fireEvent.change(textarea, { target: { value: description } });
  fireEvent.click(saveButton);

  expect(addCard).toHaveBeenCalledWith({ title, description });
  expect(getByText('Add a card...')).toBeInTheDocument();
  expect(queryByPlaceholderText('Title')).not.toBeInTheDocument();
  expect(queryByPlaceholderText('Description')).not.toBeInTheDocument();
  expect(queryByText('Save')).not.toBeInTheDocument();
  expect(queryByText('Cancel')).not.toBeInTheDocument();
});

it('can cancel adding a card', () => {
  const addCard = jest.fn();
  const { getByText, queryByText, getByPlaceholderText, queryByPlaceholderText } = render(<AddCard addCard={addCard} />);
  expect(getByText('Add a card...')).toBeInTheDocument();

  fireEvent.click(getByText('Add a card...'));
  expect(queryByText('Add a card...')).not.toBeInTheDocument();
  const input = getByPlaceholderText('Title');
  const textarea = getByPlaceholderText('Description');
  const cancelButton = getByText('Cancel');
  expect(input).toBeInTheDocument();
  expect(textarea).toBeInTheDocument();
  expect(getByText('Save')).toBeInTheDocument();
  expect(cancelButton).toBeInTheDocument();

  const title = 'some title';
  const description = 'some description';
  fireEvent.change(input, { target: { value: title } });
  fireEvent.change(textarea, { target: { value: description } });
  fireEvent.click(cancelButton);

  expect(addCard).not.toHaveBeenCalled();
  expect(getByText('Add a card...')).toBeInTheDocument();
  expect(queryByPlaceholderText('Title')).not.toBeInTheDocument();
  expect(queryByPlaceholderText('Description')).not.toBeInTheDocument();
  expect(queryByText('Save')).not.toBeInTheDocument();
  expect(queryByText('Cancel')).not.toBeInTheDocument();
});
