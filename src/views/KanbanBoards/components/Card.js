import PropTypes from 'prop-types';
import * as React from 'react';
import styled from 'styled-components';
import Block from '../../../components/layout/Block';
import Text from '../../../components/layout/Text';
import color from '../../../constants/color';
import { fontSize, fontWeight } from '../../../constants/font';
import { defaultBorderRadius } from '../../../constants/style';

const BlockWithShadow = styled(Block)`
  box-shadow: 0 0 2px rgba(0,0,0,0.2), 0 4px 8px rgba(0,0,0,0.15);
`;

const Card = ({ card }) => (
  <BlockWithShadow marginBottom={2} padding={2} bg={color.cardBg} borderRadius={defaultBorderRadius}>
    <Text fontWeight={fontWeight.semiBold} marginBottom={card.description ? 1 : 0}>{card.title}</Text>
    {card.description.length > 0 && <Text fontSize={fontSize.small} color={color.fontLighter}>{card.description}</Text>}
  </BlockWithShadow>
);

Card.propTypes = {
  card: PropTypes.object.isRequired,
};

export default Card;
