import React from 'react';
import AddList from './AddList';

it('renders and calls addList on save', () => {
  const addList = jest.fn();
  const { getByText, queryByText, getByPlaceholderText, queryByPlaceholderText } = render(<AddList addList={addList} />);
  expect(getByText('Add a list...')).toBeInTheDocument();

  fireEvent.click(getByText('Add a list...'));
  expect(queryByText('Add a list...')).not.toBeInTheDocument();
  const input = getByPlaceholderText('List title');
  const saveButton = getByText('Save');
  expect(input).toBeInTheDocument();
  expect(saveButton).toBeInTheDocument();
  expect(getByText('Cancel')).toBeInTheDocument();

  const title = 'some title';
  fireEvent.change(input, { target: { value: title } });
  fireEvent.click(saveButton);
  expect(addList).toHaveBeenCalledWith(title);
  expect(getByText('Add a list...')).toBeInTheDocument();
  expect(queryByPlaceholderText('List title')).not.toBeInTheDocument();
  expect(queryByText('Save')).not.toBeInTheDocument();
  expect(queryByText('Cancel')).not.toBeInTheDocument();
});

it('can cancel', () => {
  const addList = jest.fn();
  const { getByText, queryByText, getByPlaceholderText, queryByPlaceholderText } = render(<AddList addList={addList} />);
  expect(getByText('Add a list...')).toBeInTheDocument();

  fireEvent.click(getByText('Add a list...'));
  expect(queryByText('Add a list...')).not.toBeInTheDocument();
  const cancelButton = getByText('Cancel');
  expect(getByPlaceholderText('List title')).toBeInTheDocument();
  expect(getByText('Save')).toBeInTheDocument();
  expect(cancelButton).toBeInTheDocument();

  fireEvent.change(getByPlaceholderText('List title'), { target: { value: 'some title' } });
  fireEvent.click(cancelButton);
  expect(addList).not.toHaveBeenCalled();
  expect(getByText('Add a list...')).toBeInTheDocument();
  expect(queryByPlaceholderText('List title')).not.toBeInTheDocument();
  expect(queryByText('Save')).not.toBeInTheDocument();
  expect(queryByText('Cancel')).not.toBeInTheDocument();
});

it('can trigger save with Enter', () => {
  const addList = jest.fn();
  const { getByText, getByPlaceholderText } = render(<AddList addList={addList} />);
  expect(getByText('Add a list...')).toBeInTheDocument();

  fireEvent.click(getByText('Add a list...'));
  const input = getByPlaceholderText('List title');
  const title = 'some title';
  fireEvent.change(input, { target: { value: title } });
  fireEvent.keyDown(input, { key: 'Enter' });
  expect(addList).toHaveBeenCalledWith(title);
});

it('can trigger cancel with Escape', () => {
  const addList = jest.fn();
  const { getByText, getByPlaceholderText, queryByPlaceholderText, queryByText } = render(<AddList addList={addList} />);
  expect(getByText('Add a list...')).toBeInTheDocument();

  fireEvent.click(getByText('Add a list...'));
  const input = getByPlaceholderText('List title');
  const title = 'some title';
  fireEvent.change(input, { target: { value: title } });
  fireEvent.keyDown(input, { key: 'Escape' });
  expect(addList).not.toHaveBeenCalled();

  expect(getByText('Add a list...')).toBeInTheDocument();
  expect(queryByPlaceholderText('List title')).not.toBeInTheDocument();
  expect(queryByText('Save')).not.toBeInTheDocument();
  expect(queryByText('Cancel')).not.toBeInTheDocument();
});
