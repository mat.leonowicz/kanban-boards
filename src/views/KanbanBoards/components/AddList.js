import PropTypes from 'prop-types';
import React, { useCallback, useState } from 'react';
import useToggle from 'react-use/lib/useToggle';
import Button from '../../../components/Button';
import Input from '../../../components/Input';
import Block from '../../../components/layout/Block';
import Flex from '../../../components/layout/Flex';
import Text from '../../../components/layout/Text';
import color from '../../../constants/color';
import { fontWeight } from '../../../constants/font';
import handleEnter from '../../../utils/handleEnter';
import handleEscape from '../../../utils/handleEscape';
import { INVALID_LIST_TITLE, isListTitleValid } from '../../../utils/validation';

const AddList = ({ addList }) => {
  const [isAdding, toggleIsAdding] = useToggle(false);
  const [listTitle, setListTitle] = useState('');

  const handleInputChange = useCallback(({ target: { value } }) => setListTitle(value), []);
  const handleCancel = useCallback(() => {
    setListTitle('');
    toggleIsAdding(false);
  }, [toggleIsAdding]);
  const handleSave = useCallback(() => {
    addList(listTitle);
    toggleIsAdding(false);
    setListTitle('');
  }, [addList, listTitle, toggleIsAdding]);

  if (!isAdding) {
    return (
      <Block width="300px" paddingTop={2}>
        <Text color={color.accent} onClick={toggleIsAdding} fontWeight={fontWeight.medium}>Add a list...</Text>
      </Block>
    );
  }

  const isValid = isListTitleValid(listTitle);

  return (
    <Block width="300px">
      <Block marginBottom={1}>
        <Input
          autoFocus
          placeholder="List title"
          value={listTitle}
          onChange={handleInputChange}
          onKeyDown={(e) => {
            handleEnter(handleSave)(e);
            handleEscape(handleCancel)(e);
          }}
        />
      </Block>
      {!isValid && (
        <Text color={color.error}>{INVALID_LIST_TITLE}</Text>
      )}
      <Flex alignItems="center">
        <Button
          small
          marginRight={1}
          onClick={handleSave}
          disabled={!isValid}
        >
          Save
        </Button>
        <Button
          small
          bgColor={color.buttonBgNegative}
          onClick={handleCancel}
        >
          Cancel
        </Button>
      </Flex>
    </Block>
  );
};

AddList.propTypes = {
  addList: PropTypes.func.isRequired,
};

export default AddList;
