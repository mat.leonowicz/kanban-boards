const accent = '#00beff';
export default {
  pageBackground: '#fbfcfc',
  fontDefault: '#302f40',
  fontLighter: '#494862',
  buttonFont: '#ffffff',
  buttonBg: '#55bcff',
  buttonBgNegative: '#ff7c6c',
  inputBorder: '#c5cdd0',
  inputBorderActive: accent,
  inputBgDisabled: '#bbd1da',
  accent,
  boardBorder: '#c4deec',
  listBg: '#ffffff',
  listBorder: '#dadde0',
  cardBg: '#ffffff',
  error: '#fd5743',
};
