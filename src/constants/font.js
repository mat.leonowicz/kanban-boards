export const fontFamily = '"Montserrat", sans-serif';

export const fontSize = {
  small: '12px',
  semiSmall: '13px',
  default: '14px',
  semiBig: '16px',
  big: '18px',
  pageHeader: '24px',
};

export const fontWeight = {
  default: 400,
  medium: 500,
  semiBold: 600,
  bold: 700,
};
