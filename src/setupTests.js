// eslint-disable-next-line no-unused-vars
// noinspection ES6UnusedImports
import { cleanup, fireEvent, render } from '@testing-library/react';
import 'jest-dom/extend-expect';
import 'jest-styled-components';

global.render = render;
global.fireEvent = fireEvent;

afterEach(cleanup);
