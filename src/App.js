import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import BoardUrlManager from './containers/BoardUrlManager';
import GlobalStyles from './GlobalStyles';
import KanbanBoards from './views/KanbanBoards/KanbanBoards';

function App() {
  return (
    <Router>
      <Route
        path="/:boardId?"
        render={props => (
          <BoardUrlManager {...props}>
            {({ boardId, setBoardId }) => (
              <KanbanBoards boardId={boardId} setBoardId={setBoardId} />
            )}
          </BoardUrlManager>
        )}
      />
      <GlobalStyles />
    </Router>
  );
}

export default App;
