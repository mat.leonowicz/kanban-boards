export const createBoard = id => ({ id, title: `board title ${id}` });
export const createList = (id, boardId) => ({ id, title: `list title ${id}`, boardId });
export const createCard = (id, listId) => ({ id, title: `card title ${id}`, description: `card description ${id}`, listId });
